# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.db import models


class ResumeOwner(models.Model):
    first_name = models.CharField(max_length=25)
    middle_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    designation = models.CharField(max_length=100)
    description = models.CharField(max_length=255)

    def __str__(self):
        return '{} {} {}'.format(self.first_name, self.middle_name, self.last_name)


class ResumeOwnerDetails(models.Model):
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female')
    )
    owner = models.ForeignKey(ResumeOwner, on_delete=models.CASCADE)
    date_of_birth = models.DateField(default=None, null=True)
    place_of_birth = models.CharField(max_length=25)
    country = models.CharField(max_length=50)
    region = models.CharField(max_length=50)
    tribe = models.CharField(max_length=50)
    gender = models.CharField(max_length=25, choices=GENDER_CHOICES, default='Male')
    nationality = models.CharField(max_length=50, default='Tanzanian')
    # profile_photo = models.ImageField(upload_to='media/profile_photos/{}'.format(datetime.now()))

    def __str__(self):
        return '{} {}'.format(self.owner.first_name, self.owner.last_name)


class ResumeOwnerSocialAccounts(models.Model):
    owner = models.ForeignKey(ResumeOwner, models.CASCADE)
    instagram_url = models.URLField(default=None, null=True)
    facebook_url = models.URLField(default=None, null=True)
    youtube_url = models.URLField(default=None, null=True)
    twitter_url = models.URLField(default=None, null=True)
    linkedin_url = models.URLField(default=None, null=True)
    stackoverflow_url = models.URLField(default=None, null=True)
    github_url = models.URLField(default=None, null=True)
    bitbucket_url = models.URLField(default=None, null=True)

    def __str__(self):
        return '{} {}'.format(self.owner.first_name, self.owner.last_name)


class ResumeOwnerContacts(models.Model):
    owner = models.ForeignKey(ResumeOwner, on_delete=models.CASCADE)
    email = models.EmailField(default=None, null=True)
    mobile = models.CharField(max_length=15, null=True)
    fax = models.CharField(max_length=15)
    telephone = models.CharField(max_length=15)

    def __str__(self):
            return '{} {}'.format(self.owner.first_name, self.owner.last_name)